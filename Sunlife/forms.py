from django import forms
from django.forms import ModelForm
from django.views.generic import CreateView

from .models import SunlifePerson, AppManagement, SunlifeAddress, SunlifeProduct, HealthCheckSurvey

#this class will be handle application form
class AppManagementForm(forms.ModelForm):
    class Meta:
        model = AppManagement
        fields = '__all__'

class SunlifePersonForm(forms.ModelForm):
    A06_NgayThangNamSinh = forms.DateField(input_formats=["%d %m %Y"])
    class Meta:
        model = SunlifePerson

        fields = '__all__'

class SunlifeAddressForm(forms.ModelForm):
    
    class Meta:
        model = SunlifeAddress

        fields = '__all__'

class SunlifeProductForm(forms.ModelForm):
    
    class Meta:
        model = SunlifeProduct

        fields = '__all__'

class HealthCheckSurveyForm(forms.ModelForm):
    
    class Meta:
        model = HealthCheckSurvey

        fields = '__all__'

