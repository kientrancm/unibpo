#!/usr/bin/python
# -*- coding: utf8 -*-

from django.db import models
from django.urls import reverse

from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver


STATUS_APP_CHOISES = [
        ('N', 'Tạo mới'),
        ('S', 'Bắt đầu'),
        ('F', 'Hoàn thành'),
    ]

BIRTH_PLACES = [
    ('AG','An Giang'),
    ('BA','Bà Rịa - Vũng Tàu'),
    ('BD','Bình Định'),
    ('BG','Bắc Giang'),
    ('BH','Bình Phước'),
    ('BI','Bình Dương'),
    ('BK','Bắc Cạn'),
    ('BL','Bạc Liêu'),
    ('BN','Bắc Ninh'),
    ('BR','Bến Tre'),
    ('BT','Bình Thuận'),
    ('CB','Cao Bằng'),
    ('CM','Cà Mau'),
    ('CT','Cần Thơ'),
    ('DE','Điện Biên'),
    ('DK','Đắk Nông'),
    ('DL','Dắk Lắk'),
    ('DN','Đà Nẵng'),
    ('DO','Đồng Nai'),
    ('DT','Đồng Tháp'),
    ('GL','Gia Lai'),
    ('HA','Hậu Giang'),
    ('HB','Hòa Bình'),
    ('HC','TP. Hồ Chí Minh'),
    ('HD','Hải Dương'),
    ('HG','Hà Giang'),
    ('HI','Hà Nội'),
    ('HN','Hà Nam'),
    ('HP','Hải Phòng'),
    ('HT','Hà Tĩnh'),
    ('HU','Thừa Thiên - Huế'),
    ('HY','Hưng Yên'),
    ('KG','Kiên Giang'),
    ('KH','Khánh Hòa'),
    ('KT','Kon Tum'),
    ('LA','Long An'),
    ('LC','Lào Cai'),
    ('NA','Nghệ An'),
    ('NB','Ninh Bình'),
    ('ND','Nam Định'),
    ('NT','Ninh Thuận'),
    ('PT','Phú Thọ'),
    ('PY','Phú Yên'),
    ('QB','Quảng Bình'),
    ('QG','Quảng Ngãi'),
    ('QM','Quảng Nam'),
    ('QN','Quảng Ninh'),
    ('QT','Quảng Trị'),
    ('SL','Sơn La'),
    ('ST','Sóc Trăng'),
    ('TA','Thanh Hóa'),
    ('TG','Tiền Giang'),
    ('TH','Thái Bình'),
    ('TN','Thái Nguyên'),
    ('TQ','Tuyên Quang'),
    ('TV','Trà Vinh'),
    ('TY','Tây Ninh'),
    ('VL','Vĩnh Long'),
    ('VP','Vĩnh Phúc'),
    ('XX','XX'),
    ('YB','Yên Bái'),
    ('LD','Lâm Đồng'),
    ('LI','Lai Châu'),
    ('LS','Lạng Sơn'),
    ('US','United States'),
    ('GS','GA Sóc Sơn'),
    ('GT','GA Tuy An'),
    ('GN','GA Núi Thành'),
    ('GC','GA Cà Mau'),
    ('BB','GA Bà Rịa'),
    ('H1','GA Hải Dương 1'),
    ('H2','GA Hải Dương 2'),
    ('HM','GA Hoàng Mai'),
    ('A1','TCA Hải Dương'),
    ('A2','TCA Hà Nội'),
    ('A3','TCA Bắc Giang'),
    ('A4','TCA Nghệ An'),
    ('A5','TCA Phú Thọ'),
    ('A6','TCA Thái Bình'),
    ('A7','TCA Vĩnh Phúc'),
    ('A8','TCA Hưng Yên'),
    ('A9','TCA Thanh Hóa'),
    ('B1','TCA Đà Nẵng'),
    ('B2','TCA Dắk Lắk'),
    ('B3','TCA Gia Lai'),
    ('B4','GA Tĩnh Gia'),
    ('B5','GA Bắc Từ Liêm'),
    ('B6','TCA Quảng Ngãi'),
    ('B7','TCA Dắk Nông'),
    ('B8','TCA Quảng Nam'),
    ('B9','TCA Quảng Trị'),
    ('C1','TCA Đồng Nai'),
    ('C2','TCA Hồ Chí Minh'),
    ('C3','TCA Cần Thơ'),
    ('C4','TCA Bình Phước'),
    ('C5','TCA Vũng Tàu'),
    ('C6','GA Bà Rịa 2'),
    ('C7','GA Cần Thơ'),
    ('AN','GA An Nhơn'),
    ('C8','TCA Quảng Ninh'),
    ('C9','TCA Hải Phòng'),
    ('D1','TCA Bỉm Sơn'),
    ('D2','TCA Quảng Bình'),
    ('D3','GA Hai Bà Trưng'),
    ('D4','GA Nga Sơn'),
    ('D5','TCA Sơn Tây'),
    ('D6','TCA Lai Châu'),
    ('D7','TCA Tĩnh Gia'),
    ('D8','TCA Ninh Bình'),
    ('D9','TCA Nam Định'),
    ('E1','TCA Bình Dương'),
    ('E2','Head Office'),
    ('E3','iVND Hà Nội'),
    ('E4','iVND Hồ Chí Minh'),
]

NATIONALITY = [
    ('AA','Angola'),
    ('AB','Albania'),
    ('AC','Ascension Island (UK)'),
    ('AD','Abu Dhabi'),
    ('AE','Antilles'),
    ('AF','Afghanistan'),
    ('AG','Argentina'),
    ('AI','Anguilla (UK)'),
    ('AL','Algeria'),
    ('AM','American Samoa'),
    ('AN','Antigua'),
    ('AO','Andora'),
    ('AR','Armenia'),
    ('AV','Azervaijan'),
    ('AZ','Azores (Port.)'),
    ('B1','Belarus'),
    ('BA','Barbados'),
    ('BB','Bhutan'),
    ('BC','Belize'),
    ('BD','Bangladesh'),
    ('BE','Belgium'),
    ('BF','Burkina Faso'),
    ('BG','Bulgaria'),
    ('BH','British Honduras'),
    ('BI','Bahrain'),
    ('BL','Bolivia'),
    ('BM','Bahamas'),
    ('BN','Brunei'),
    ('BO','Bosnia - Herzegovina'),
    ('BP','Belau (also Palau)'),
    ('BR','Bermuda'),
    ('BT','British Indian Ocean Territory'),
    ('BU','Myanmar'),
    ('BV','Burundi'),
    ('BW','Botswana'),
    ('BX','Benin'),
    ('BY','British Antarctic Territory'),
    ('BZ','Brazil'),
    ('C1','Chaozhou'),
    ('C2','Chongqing'),
    ('CA','Chad'),
    ('CB','Cuba'),
    ('CC','Curacao'),
    ('CD','Cambodia'),
    ('CE','Sri Lanka'),
    ('CF','Central African Republic'),
    ('CG','Congo'),
    ('CH','China'),
    ('CI','Canary Islands'),
    ('CJ','Grand Cayman Islands'),
    ('CK','Comoros'),
    ('CL','Chile'),
    ('CM','Columbia'),
    ('CN','Canada'),
    ('CO','Cote D`Ivoire'),
    ('CP','Cyprus'),
    ('CQ','Cameroon'),
    ('AS','Australia'),
    ('AT','Austria'),
    ('AU','Aruba'),
    ('CU','Czech Republic'),
    ('CV','Cape Verde Island'),
    ('CY','Cayman Islands'),
    ('CZ','Czechoslovakia'),
    ('D1','Dongguan'),
    ('D2','Dubai'),
    ('DG','Diego Garcia'),
    ('DJ','Republic of Djibouti'),
    ('DK','Denmark'),
    ('DM','Dominica'),
    ('DR','Dominican Republic'),
    ('DS','Dominican Republic Special'),
    ('EC','Ecuador'),
    ('EG','Egypt'),
    ('EI','Republic of Ireland'),
    ('EL','El Salvador'),
    ('EN','England'),
    ('EQ','Equatorial Guinea'),
    ('ER','Eritrea'),
    ('ES','Estonia'),
    ('ET','Ethiopia'),
    ('F1','Foshan'),
    ('FA','Faeroe Islands'),
    ('FG','French Guiana'),
    ('FI','Finland'),
    ('FJ','Fiji Island'),
    ('FK','Falkland Islands'),
    ('FP','French Polynesia'),
    ('FR','France'),
    ('FS','Foreign Special'),
    ('FW','French West Indies'),
    ('G1','Guangzhou'),
    ('GA','Gambia'),
    ('GB','Gabon'),
    ('GC','Gold Coast'),
    ('GD','Guadeloupe'),
    ('GE','Greece'),
    ('GG','Georgia'),
    ('GH','Ghana'),
    ('GI','Gibraltar'),
    ('GL','Greenland'),
    ('GM','Germany'),
    ('GN','Guyana'),
    ('GR','Grenada'),
    ('CR','Costa Rica'),
    ('CS','Cuba Special'),
    ('CT','Croatia'),
    ('GX','Guam'),
    ('GY','Guernsey'),
    ('H1','Huizhou'),
    ('HK','Hong Kong'),
    ('HL','Holland'),
    ('HN','Honduras'),
    ('HS','Spanish Sahara'),
    ('HT','Haiti'),
    ('HU','Hungary'),
    ('IC','Iceland'),
    ('II','India'),
    ('IL','Ireland'),
    ('IM','Isle of Man'),
    ('IO','Indonesia'),
    ('IQ','Iraq'),
    ('IR','Iran'),
    ('IS','Israel'),
    ('IT','Italy'),
    ('IV','Ivory Coast'),
    ('J1','Jiangmen'),
    ('JA','Jamaica'),
    ('JO','Jordan'),
    ('JP','Japan'),
    ('JY','Jersey'),
    ('KB','Kiribati'),
    ('KG','Kirghizia'),
    ('KN','Kenya'),
    ('KO','South Korea'),
    ('KR','North Korea'),
    ('KS','Krygstan'),
    ('KU','Kuwait'),
    ('KZ','Kazakhstan'),
    ('LC','Liechtenstein'),
    ('LE','Lebanon'),
    ('LH','Lesotho'),
    ('LI','Liberia'),
    ('LO','Laos'),
    ('LS','Sierra Leone'),
    ('LT','Lithuania'),
    ('LV','Latvia'),
    ('LX','Luxembourg'),
    ('LY','Libya'),
    ('MA','Mauritania'),
    ('MB','Maldives'),
    ('GS','Guinea - Bissau'),
    ('GT','Guatemala'),
    ('GU','Guinea'),
    ('MG','Micronesia'),
    ('MH','Marshall Island'),
    ('MI','Mali'),
    ('MK','Morocco'),
    ('ML','Malta'),
    ('MM','San Marino'),
    ('MN','Monaco'),
    ('MO','Mongolia'),
    ('MQ','Martinique'),
    ('MR','Montserrat'),
    ('MS','Madagascar'),
    ('MT','Montenegro'),
    ('MU','Mauritius'),
    ('MV','Moldova'),
    ('MW','Malawi'),
    ('MX','Mexico'),
    ('MY','Malaysia'),
    ('MZ','Mozambique'),
    ('NA','Netherlands Antilles'),
    ('NB','Namibia'),
    ('NC','New Caledonia'),
    ('NE','Niger'),
    ('NG','Netherlands Guiana'),
    ('NI','Northern Ireland'),
    ('NL','Netherlands'),
    ('NN','Nunavut (N, W, T-E)'),
    ('NP','Nepal'),
    ('NQ','Nauru'),
    ('NR','Nigeria'),
    ('NU','Nicaragua'),
    ('NW','Norway'),
    ('NZ','New Zealand'),
    ('OA','Samoa'),
    ('OM','Oman'),
    ('OS','Somalia'),
    ('P1','Province of Hainan'),
    ('PG','Paraguay'),
    ('PH','Philippines'),
    ('PK','Pakistan'),
    ('PL','Poland'),
    ('PM','Panama'),
    ('PN','Papua New Guinea'),
    ('PO','Port Moresby'),
    ('PR','Puerto Rico'),
    ('MC','Macau'),
    ('MD','Madeira'),
    ('ME','Macedonia'),
    ('QT','Qatar'),
    ('R1','Rwanda'),
    ('RE','Reunion'),
    ('RO','Romania'),
    ('RP','Republic of Palau'),
    ('RU','Russia'),
    ('S1','Shanghai'),
    ('S2','Shaquan'),
    ('S3','Shenyang'),
    ('S4','Shenzen'),
    ('SA','South Africa'),
    ('SB','Saudi Arabia'),
    ('SD','Sudan'),
    ('SE','Sweden'),
    ('SF','Southwest Africa'),
    ('SG','Senegal'),
    ('SH','Spanish Honduras'),
    ('SI','Saipan'),
    ('SJ','Serbia - Monterregra'),
    ('SK','Solomon Island'),
    ('SL','St. Lucia'),
    ('SM','St. Martin'),
    ('SN','Spain'),
    ('SO','Sultanate of Oman'),
    ('SP','Singapore'),
    ('SQ','Seychelles'),
    ('SR','Salvador'),
    ('SS','St. Kitts'),
    ('ST','Scotland'),
    ('SU','Surinam'),
    ('SV','St. Vincent'),
    ('SW','Sarawak'),
    ('SX','Slovenia'),
    ('SY','Syria'),
    ('SZ','Switzerland'),
    ('T1','Tianjim'),
    ('TA','Tanzania'),
    ('TC','Turks & Caicos Island'),
    ('TH','Thailand'),
    ('TI','Tibet'),
    ('TJ','Tajikistan'),
    ('TK','Turkey'),
    ('TL','Tokelau'),
    ('TM','Turkmenistan'),
    ('PS','Palestinian State'),
    ('PT','Portugal'),
    ('PU','Peru'),
    ('TR','Trinidad'),
    ('TS','Tuinisia'),
    ('TV','Tuvalu'),
    ('TW','Taiwan'),
    ('UA','United Arab Emirates'),
    ('UG','Uganda'),
    ('UK','United Kingdom'),
    ('UR','Ukraine'),
    ('US','United States'),
    ('UV','Upper Volta'),
    ('UY','Uruguay'),
    ('UZ','Uzbekistan'),
    ('VC','Vatican City'),
    ('VE','Venezuela'),
    ('VI','Virgin Islands'),
    ('VK','Slovak Republic'),
    ('VN','Việt Nam'),
    ('VU','Vanuatu'),
    ('W1','Wuhan'),
    ('WA','Wales'),
    ('WE','Western Samoa'),
    ('WI','West Indies'),
    ('WS','Western Sahara'),
    ('X1','Xianmen in Fujian Prov.'),
    ('YE','Yemen'),
    ('YU','Yugoslavia'),
    ('Z1','Zhanjiang'),
    ('Z2','Zhaoqing'),
    ('Z3','Zhongshan'),
    ('Z4','Zhubai of Guangdong'),
    ('ZA','Zaire'),
    ('ZB','Zambia'),
    ('ZM','Zimbabwe'),
    ('ZW','Swaziland'),
    ('TN','Tonga'),
    ('TO','Togo'),
    ('TP','Sao Tome and Principe'),
]

GIOI_TINH = [
    ('M','Nam'),
    ('F','Nữ'),
    ('C','Công Ty'),
]

YES_NO_CHOISES = [
        ('Y', '1. Có'),
        ('N', '2. Không'),
        ('', '3.Không chọn'),
]

HON_NHAN_STATUS = [
    ('D', 'Đã ly dị'),
    ('S', 'Độc thân'),
    ('M', 'Kết hôn'),
    ('W', 'Góa phụ'),
    ('O', 'Khác'),
]

ID_DOCUMENT_TYPE = [
    ('S', 'CMND'),
    ('P', 'Passport'),
    ('O', 'Khác')
]
class AppManagement(models.Model):
    
    AppID = models.CharField(max_length=30)
    pub_date = models.DateTimeField('Date published', auto_now_add=True)
    status = models.CharField(max_length=1, choices=STATUS_APP_CHOISES, default='N')
    updated_at = models.DateTimeField(auto_now = True)
    def __str__(self):
        return self.AppID.__str__()

#Personal
class SunlifePerson(models.Model):
    App_ID = models.ForeignKey(AppManagement, on_delete=models.CASCADE, help_text="Lựa chọn chính xác hồ sơ bạn đang nhập")    
    A02_MS_TVTC  = models.CharField('Mã số tư vấn tài chính',max_length=60)
    A03_Ho  = models.CharField('Họ',max_length=60)
    A04_TenLot  = models.CharField('Tên lót',max_length=60)
    A05_Ten  = models.CharField('Tên',max_length=60)
    A06_NgayThangNamSinh  = models.CharField('Ngày tháng năm sinh',max_length=60)
    A07_NoiSinh  = models.CharField('Nơi sinh',max_length=2, choices=BIRTH_PLACES)
    A08_QuocTich01  = models.CharField('Quốc tịch 1',max_length=2, choices=NATIONALITY)
    A09_QuocTich02  = models.CharField('Quốc tịch 2',max_length=2, choices=NATIONALITY, default= None, blank=True)
    A10_GioiTinh  = models.CharField('Giới tính',max_length=1, choices = GIOI_TINH)
    A11_TinhTrangHonNhan  = models.CharField('Tình trạng hôn nhân',max_length=1, choices= HON_NHAN_STATUS)
    A12_LoaiGiayTo  = models.CharField('Loại giấy tờ',max_length=1, choices=ID_DOCUMENT_TYPE)
    A13_SoGiayToTuyThan  = models.CharField('Số giấy tờ tùy thân',max_length=20)
    A14_NgayCap  = models.DateField('Ngày cấp')
    A31_MoiQH_NDBH_BMBH  = models.CharField(max_length=60)
    A113_PO_LI = models.CharField(max_length=60)
    A116_Ho = models.CharField(max_length=60)
    A117_TenLot = models.CharField(max_length=60)
    A118_Ten = models.CharField(max_length=60)
    A119_DoB = models.CharField(max_length=60)
    A120_NoiSinh = models.CharField(max_length=60)
    A121_QT01= models.CharField(max_length=60)
    A122_QT02 = models.CharField(max_length=60)
    A123_GioiTinh = models.CharField(max_length=60)
    A124_TinhTrangHonNhan = models.CharField(max_length=60)
    A125_LoaiGiayTo = models.CharField(max_length=60)
    A126_SoGiayToTuyThan = models.CharField(max_length=60)
    A127_NgayCap = models.CharField(max_length=60)
    A144_MoiQH_NDBH_BMBH = models.CharField(max_length=60)
    A39_HoTen  = models.CharField(max_length=60)
    A40_NgaySinh  = models.CharField(max_length=60)
    A41_TyLeThuHuong  = models.CharField(max_length=60)
    A42_MoiQH  = models.CharField(max_length=60)
    A43_TenToChuc  = models.CharField(max_length=60)
    A44_TyLeThuHuong  = models.CharField(max_length=60)
    A45_HoTen  = models.CharField(max_length=60)
    A46_NgaySinh  = models.CharField(max_length=60)
    A47_MoiQH  = models.CharField(max_length=60)

    def __str__(self):
        return self.App_ID.__str__()

#Address
class SunlifeAddress(models.Model):
    App_ID = models.ForeignKey(AppManagement, on_delete=models.CASCADE)
    A15_Dien_Thoai_Nha_Rieng = models.CharField(max_length=60)
    A16_Dien_Thoai_Di_Dong = models.CharField(max_length=60)
    A17_Dia_Chi_Email = models.CharField(max_length=60)
    A18_Nghe_Nghiep = models.CharField(max_length=60)
    A19_Co_Quan = models.CharField(max_length=60)
    A20_Thu_Nhap_Nam = models.CharField(max_length=60)
    A21_DC_Thuong_Tru = models.CharField(max_length=60)
    A22_So_Nha = models.CharField(max_length=60)
    A23_Phuong_Xa = models.CharField(max_length=60)
    A24_Quan_Huyen = models.CharField(max_length=60)
    A25_Tinh_ThanhPho = models.CharField(max_length=60)
    #A26_Dc_Lien_Lac_ThuTin = models.CharField(max_length=60)
    A27_Sonha_Duong_NhanThu = models.CharField(max_length=60)
    A28_Phuong_Xa = models.CharField(max_length=60)
    A29_Quan_Huyen = models.CharField(max_length=60)
    A30_Tinh_TP = models.CharField(max_length=60)
    A128_DienThoai_NhaRieng = models.CharField(max_length=60)
    A129_DTDD = models.CharField(max_length=60)
    A130_Email = models.CharField(max_length=60)
    A131_Nghe_Nghiep = models.CharField(max_length=60)
    A132_Cty = models.CharField(max_length=60)
    A133_Thu_Nhap_Nam = models.CharField(max_length=60)
    #A134_DC_Thuong_Chu 
    A135_SoNha_Duong_LI = models.CharField(max_length=60)
    A136_Phuong_Xa_LI = models.CharField(max_length=60)
    A137_Quan_Huyen_LI = models.CharField(max_length=60)
    A138_Tinh_TP_LI = models.CharField(max_length=60)
    def __str__(self):
        return self.App_ID.__str__()
    

#Product
class SunlifeProduct(models.Model):
    App_ID = models.ForeignKey(AppManagement, on_delete=models.CASCADE)
    A507_Muc_Tieu_TC = models.CharField(max_length=60)
    A509_Khoan_ThucHien = models.CharField(max_length=60)
    A510_Noi_Nhan_HD = models.CharField(max_length=60)
    A511_Nguon_KhaiThac_BG = models.CharField(max_length=60)
    A500_USTIN = models.CharField(max_length=60)
    A501_TypeIRS = models.CharField(max_length=60)
    A502_IRSStatus = models.CharField(max_length=60)
    A503_Date_IRS = models.CharField(max_length=60)
    A32_FATCA = models.CharField(max_length=60)
    A33_MST_HK = models.CharField(max_length=60)
    A34_BMBH_ToChuc = models.CharField(max_length=60)
    A35_SP_PO = models.CharField(max_length=60)
    A512_So_Tien_BH_PO = models.CharField(max_length=60)
    A517_ThoiHan_PO = models.CharField(max_length=60)
    A603_Sun_SongChuDong = models.CharField(max_length=60)
    A36_Sp_BHBS = models.CharField(max_length=60)
    A37_DinhKy_DongPhi_PO = models.CharField(max_length=60)
    A504_SP_BHC_LI = models.CharField(max_length=60)
    A505_So_Tien_BHC_LI = models.CharField(max_length=60)
    A518_Thoi_Han_DongPhi_LI = models.CharField(max_length=60)
    A38_HieuLuc_HDBH = models.CharField(max_length=60)
    A509_NgayNhan_HSYCBH = models.CharField(max_length=60)
    A601_NgayKy_HSYCBH = models.CharField(max_length=60)
    A602_Ngay_NopTien_TrenPhieuThu = models.CharField(max_length=60)
    A604_ReferName = models.CharField(max_length=60)
    A48_HDKhac = models.CharField(max_length=60)
    A49_CtyBH_PO = models.CharField(max_length=60)
    A50_Ten_SP_PO = models.CharField(max_length=60)
    A51_SoTien_PO = models.CharField(max_length=60)
    A52_Nam_Phat_Hanh = models.CharField(max_length=60)
    A53_Tinh_Trang_HD_PO = models.CharField(max_length=60)
    A606_HDBH_ConHieuLuc = models.CharField(max_length=60)
    A600_Cty_BH_LI = models.CharField(max_length=60)
    A513_TenSP_BH_LI = models.CharField(max_length=60)
    A514_So_Tien_LI = models.CharField(max_length=60)
    A515_Nam_PhatHanh_LI = models.CharField(max_length=60)
    A516_Tinh_Trang_LI = models.CharField(max_length=60)
    A54_Du_Dinh_ThayThe = models.CharField(max_length=60)
    A55_Tinh_Trang = models.CharField(max_length=60)
    A56_Ke_Khai = models.CharField(max_length=60)
    A508_Ngay_Ky_TVTC = models.CharField(max_length=60)
    def __str__(self):
        return self.App_ID.__str__()


class HealthCheckSurvey(models.Model):
    App_ID = models.ForeignKey(AppManagement, on_delete=models.CASCADE)
    A57_field = models.CharField(max_length=1, default = "N", choices=YES_NO_CHOISES)
    A58_field = models.CharField(max_length=1, default = "N", choices=YES_NO_CHOISES)
    A59_field = models.CharField(max_length=1, default = "N", choices=YES_NO_CHOISES)
    A60_field = models.CharField(max_length=60, default = "N")
    A61_field = models.CharField(max_length=60, default = "N")
    A62_field = models.CharField(max_length=60, default = "N")
    A63_field = models.CharField(max_length=60, default = "N")
    A64_field = models.CharField(max_length=60, default = "N")
    A65_field = models.CharField(max_length=60, default = "N")
    A66_field = models.CharField(max_length=60, default = "N")
    A67_field = models.CharField(max_length=60, default = "N")
    A519_field = models.CharField(max_length=60, default = "Bia nhẹ", help_text="Bia nhẹ, Rượu nhẹ, Rượu mạnh")
    A68_field = models.CharField(max_length=60, default = "N")
    A69_field = models.CharField(max_length=60, default = "D")
    A70_field = models.CharField(max_length=60, default = "N")
    A71_field = models.CharField(max_length=60, default = "N")
    A72_field = models.CharField(max_length=60, help_text="Number")
    A73_field = models.CharField(max_length=60, help_text="Number")
    A74_field = models.CharField(max_length=60, default = 'N')
    A75_field = models.CharField(max_length=60, default = 'N')
    A76_field = models.CharField(max_length=60, default = 'N')
    A77_field = models.CharField(max_length=60, default = 'N')
    A78_field = models.CharField(max_length=60, default = 'N')
    A79_field = models.CharField(max_length=60, default = 'N')
    A80_field = models.CharField(max_length=60, default = 'N')
    A81_field = models.CharField(max_length=60, default = 'N')
    A82_field = models.CharField(max_length=60, default = 'N')
    A83_field = models.CharField(max_length=60, default = 'N')
    A84_field = models.CharField(max_length=60, default = 'N')
    A85_field = models.CharField(max_length=60, default = 'N')
    A86_field = models.CharField(max_length=60, default = 'N')
    A87_field = models.CharField(max_length=60, default = 'N')
    A88_field = models.CharField(max_length=60, default = 'N')
    A89_field = models.CharField(max_length=60, default = 'N')
    A90_field = models.CharField(max_length=60, default = 'N')
    A91_field = models.CharField(max_length=60, default = 'N')
    A92_field = models.CharField(max_length=60, default = 'N')
    A93_field = models.CharField(max_length=60, default = 'N')
    A94_field = models.CharField(max_length=60, default = 'N')
    A95_field = models.CharField(max_length=60, default = 'N')
    A96_field = models.CharField(max_length=60, default = 'N')
    A97_field = models.CharField(max_length=60, default = 'N')
    A98_field = models.CharField(max_length=60, default = 'N')
    A99_field = models.CharField(max_length=60, default = 'N')
    A100_field = models.CharField(max_length=60, default = 'N')
    A101_field = models.CharField(max_length=60, default = 'N')
    A102_field = models.CharField(max_length=60, default = 'N')
    A103_field = models.CharField(max_length=60, default = 'N')
    A104_field = models.CharField(max_length=60, default = 'N')
    A105_field = models.CharField(max_length=60, default = 'N')
    A106_field = models.CharField(max_length=60, default = 'N')
    A107_field = models.CharField(max_length=60, default = 'N')
    A108_field = models.CharField(max_length=60, default = 'N')
    A109_field = models.CharField(max_length=60, default = 'N')
    A110_field = models.CharField(max_length=60, default = 'N')
    A111_field = models.CharField(max_length=60, default = 'N')
    A112_field = models.CharField(max_length=60, default = 'N')
    A145_field = models.CharField(max_length=60, default = 'N')
    A146_field = models.CharField(max_length=60, help_text = "Thanh Pho")
    A147_field = models.CharField(max_length=60)
    A148_field = models.CharField(max_length=60)
    A149_field = models.CharField(max_length=60)
    A150_field = models.CharField(max_length=60)
    A151_field = models.CharField(max_length=60)
    A152_field = models.CharField(max_length=60)
    A153_field = models.CharField(max_length=60)
    A154_field = models.CharField(max_length=60)
    A155_field = models.CharField(max_length=60)
    A520_field = models.CharField(max_length=60)
    A156_field = models.CharField(max_length=60)
    A157_field = models.CharField(max_length=60)
    A158_field = models.CharField(max_length=60)
    A159_field = models.CharField(max_length=60)
    A160_field = models.CharField(max_length=60)
    A161_field = models.CharField(max_length=60)
    A162_field = models.CharField(max_length=60)
    A163_field = models.CharField(max_length=60)
    A164_field = models.CharField(max_length=60)
    A165_field = models.CharField(max_length=60)
    A166_field = models.CharField(max_length=60)
    A167_field = models.CharField(max_length=60)
    A168_field = models.CharField(max_length=60)
    A169_field = models.CharField(max_length=60)
    A170_field = models.CharField(max_length=60)
    A171_field = models.CharField(max_length=60)
    A172_field = models.CharField(max_length=60)
    A173_field = models.CharField(max_length=60)
    A174_field = models.CharField(max_length=60)
    A175_field = models.CharField(max_length=60)
    A176_field = models.CharField(max_length=60)
    A177_field = models.CharField(max_length=60)
    A178_field = models.CharField(max_length=60)
    A179_field = models.CharField(max_length=60)
    A180_field = models.CharField(max_length=60)
    A181_field = models.CharField(max_length=60)
    A182_field = models.CharField(max_length=60)
    A183_field = models.CharField(max_length=60)
    A184_field = models.CharField(max_length=60)
    A185_field = models.CharField(max_length=60)
    A186_field = models.CharField(max_length=60)
    A187_field = models.CharField(max_length=60)
    A188_field = models.CharField(max_length=60)
    A189_field = models.CharField(max_length=60)
    A190_field = models.CharField(max_length=60)
    A191_field = models.CharField(max_length=60)
    A192_field = models.CharField(max_length=60)
    A193_field = models.CharField(max_length=60)
    A194_field = models.CharField(max_length=60)
    A195_field = models.CharField(max_length=60)
    A196_field = models.CharField(max_length=60)
    A01_So_HSYCBH = models.CharField(max_length=60, help_text="Hợp đồng bảo hiểm số/Số HSYCBH", default='', blank=True)
    def __str__(self):
        return self.App_ID.__str__()

