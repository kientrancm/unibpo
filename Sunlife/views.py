from django.shortcuts import render
from django.http import HttpResponse
from .forms import SunlifePersonForm, AppManagementForm, SunlifeAddressForm, SunlifeProductForm, HealthCheckSurveyForm


from django.contrib import messages


def Sunlife(request):
    return render(request, 'sunlife.html')

def AppManagementForm_View(request):
    if request.method == 'POST':
        form = AppManagementForm(request.POST)
        if form.is_valid():
            form.save()
            form = AppManagementForm()

        return render(request, 'form.html', {'form': form})

    else:    
        form = AppManagementForm()
        return render(request, 'form.html', {'form': form})


def SunlifePersonForm_View(request):
    #return HttpResponse('Trang thong tin co ban')
    if request.method == 'POST':
        form = SunlifePersonForm(request.POST)
        if form.is_valid():
            form.save()
        
    form = SunlifePersonForm()

    return render(request, 'form.html', {'form': form})

def SunlifeAddressForm_View(request):
    if request.method == 'POST':
        form = SunlifeAddressForm(request.POST)
        if form.is_valid():
            form.save()

    form = SunlifeAddressForm()
    return render(request, 'form.html', {'form': form})


def SunlifeProductForm_View(request):
    if request.method == 'POST':
        form = SunlifePersonForm(request.POST)
        if form.is_valid():
            form.save()

    form = SunlifePersonForm()
    return render(request, 'form.html', {'form': form})


def HealthCheckSurveyForm_View(request):
    if request.method == 'POST':
        form = HealthCheckSurveyForm(request.POST)
        if form.is_valid():
            form.save()

    form = HealthCheckSurveyForm()
    return render(request, 'form.html', {'form': form})

