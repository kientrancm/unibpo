from django.urls import path
from . import views

urlpatterns = [
    path('', views.Sunlife),
    path('app', views.AppManagementForm_View),
    path('person', views.SunlifePersonForm_View),
    path('address', views.SunlifeAddressForm_View),
    path('product', views.SunlifeProductForm_View),
    path('others', views.HealthCheckSurveyForm_View),
]

