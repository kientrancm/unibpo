# Generated by Django 2.2.7 on 2019-12-02 09:59

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Sunlife', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='sunlife',
            old_name='A02_MS_TVTC',
            new_name='A02_field',
        ),
        migrations.RenameField(
            model_name='sunlife',
            old_name='A03_Ho',
            new_name='A03_field',
        ),
        migrations.RenameField(
            model_name='sunlife',
            old_name='A04_TenLot',
            new_name='A04_field',
        ),
        migrations.RenameField(
            model_name='sunlife',
            old_name='A05_Ten',
            new_name='A05_field',
        ),
        migrations.RenameField(
            model_name='sunlife',
            old_name='A06_DOB',
            new_name='A06_field',
        ),
        migrations.RenameField(
            model_name='sunlife',
            old_name='A07_NoiSinh',
            new_name='A07_field',
        ),
        migrations.RenameField(
            model_name='sunlife',
            old_name='A08_QuocTich1',
            new_name='A08_field',
        ),
        migrations.RenameField(
            model_name='sunlife',
            old_name='A09_QuocTich2',
            new_name='A09_field',
        ),
    ]
