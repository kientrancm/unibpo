from django.contrib import admin

from .models import AppManagement, SunlifePerson, SunlifeAddress, SunlifeProduct, HealthCheckSurvey

admin.site.register(AppManagement)
admin.site.register(SunlifePerson)
admin.site.register(SunlifeAddress)
admin.site.register(SunlifeProduct)
admin.site.register(HealthCheckSurvey)
